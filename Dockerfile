FROM golang:1.23.1-alpine AS build

RUN apk add --update --no-cache git

COPY ./api /go/src/scheme-management-process/api
COPY ./cmd /go/src/scheme-management-process/cmd
COPY ./internal /go/src/scheme-management-process/internal
COPY ./pkg /go/src/scheme-management-process/pkg
COPY ./go.mod /go/src/scheme-management-process/
COPY ./go.sum /go/src/scheme-management-process/
WORKDIR /go/src/scheme-management-process
RUN go mod download \
 && go build -o dist/bin/scheme-management-process ./cmd/scheme-management-process

# Release binary on latest alpine image.
FROM alpine:3.20

ARG USER_ID=10001
ARG GROUP_ID=10001

COPY --from=build /go/src/scheme-management-process/dist/bin/scheme-management-process /usr/local/bin/scheme-management-process
COPY --from=build /go/src/scheme-management-process/api/openapi.json /api/openapi.json
COPY --from=build /go/src/scheme-management-process/api/openapi.yaml /api/openapi.yaml

# Add non-priveleged user. Disabled for openshift
RUN addgroup -g "$GROUP_ID" appgroup \
 && adduser -D -u "$USER_ID" -G appgroup appuser
USER appuser
HEALTHCHECK none
CMD ["/usr/local/bin/scheme-management-process"]
