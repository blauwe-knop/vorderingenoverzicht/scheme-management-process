package events

var (
	SMP_1 = NewEvent(
		"smp_1",
		"started listening",
		Low,
	)
	SMP_2 = NewEvent(
		"smp_2",
		"server closed",
		High,
	)
	SMP_3 = NewEvent(
		"smp_3",
		"received request for openapi spec as JSON",
		Low,
	)
	SMP_4 = NewEvent(
		"smp_4",
		"sent response with openapi spec as JSON",
		Low,
	)
	SMP_5 = NewEvent(
		"smp_5",
		"received request for openapi spec as YAML",
		Low,
	)
	SMP_6 = NewEvent(
		"smp_6",
		"sent response with openapi spec as YAML",
		Low,
	)
	SMP_7 = NewEvent(
		"smp_7",
		"failed to read openapi.json file",
		High,
	)
	SMP_8 = NewEvent(
		"smp_8",
		"failed to write fileBytes to response",
		High,
	)
	SMP_9 = NewEvent(
		"smp_9",
		"failed to read openapi.yaml file",
		High,
	)
	SMP_10 = NewEvent(
		"smp_10",
		"received request to approve organization",
		Low,
	)
	SMP_11 = NewEvent(
		"smp_11",
		"sent response with updated organization",
		Low,
	)
	SMP_12 = NewEvent(
		"smp_12",
		"received request to delete organization",
		Low,
	)
	SMP_13 = NewEvent(
		"smp_13",
		"sent response with deleted organization",
		Low,
	)
	SMP_14 = NewEvent(
		"smp_14",
		"failed to update organization",
		High,
	)
	SMP_15 = NewEvent(
		"smp_15",
		"failed to encode response payload",
		High,
	)
	SMP_16 = NewEvent(
		"smp_16",
		"failed to delete organization",
		High,
	)
	SMP_17 = NewEvent(
		"smp_17",
		"failed to get organization",
		High,
	)
)
