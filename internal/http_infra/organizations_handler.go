// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-management-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-management-process/pkg/events"
)

func handlerApproveOrganization(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	schemeUseCase, _ := context.Value(schemeUseCaseKey).(*usecases.SchemeUseCase)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.SMP_10
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	updatedOrganization, err := schemeUseCase.ApproveOrganization(chi.URLParam(request, "oin"))
	if err != nil {
		event := events.SMP_14
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*updatedOrganization)
	if err != nil {
		event := events.SMP_15
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SMP_11
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerDeleteOrganization(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	schemeUseCase, _ := context.Value(schemeUseCaseKey).(*usecases.SchemeUseCase)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.SMP_12
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	organization, err := schemeUseCase.DeleteOrganization(chi.URLParam(request, "oin"))
	if err != nil {
		event := events.SMP_16
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*organization)
	if err != nil {
		event := events.SMP_15
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SMP_13
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}
