// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"net/http"
	"os"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-management-process/pkg/events"
)

func handlerJson(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.SMP_3
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	fileBytes, err := os.ReadFile("/api/openapi.json")
	if err != nil {
		event = events.SMP_7
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		panic(err)
	}

	responseWriter.WriteHeader(http.StatusOK)
	responseWriter.Header().Set("Content-Type", "application/json")

	_, err = responseWriter.Write(fileBytes)
	if err != nil {
		event = events.SMP_8
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SMP_4
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerYaml(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.SMP_5
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	fileBytes, err := os.ReadFile("/api/openapi.yaml")
	if err != nil {
		event = events.SMP_9
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		panic(err)
	}

	responseWriter.WriteHeader(http.StatusOK)
	responseWriter.Header().Set("Content-Type", "application/octet-stream")

	_, err = responseWriter.Write(fileBytes)
	if err != nil {
		event = events.SMP_8
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SMP_6
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}
