// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package usecases

import (
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"go.uber.org/zap"

	servicemodel "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/model"
	serviceRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/repositories"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-management-process/pkg/events"
)

type SchemeUseCase struct {
	schemeRepository serviceRepositories.SchemeRepository
	Logger           *zap.Logger
}

func NewSchemeUseCase(logger *zap.Logger, schemeRepository serviceRepositories.SchemeRepository) *SchemeUseCase {
	return &SchemeUseCase{
		schemeRepository: schemeRepository,
		Logger:           logger,
	}
}

func (uc *SchemeUseCase) ApproveOrganization(oin string) (*servicemodel.Organization, error) {
	organization, err := uc.schemeRepository.GetOrganization(oin)
	if err != nil {
		event := events.SMP_17
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return nil, err
	}

	organization.Approved = true

	responseOrganization, err := uc.schemeRepository.UpdateOrganization(oin, *organization)

	return responseOrganization, err
}

func (uc *SchemeUseCase) DeleteOrganization(oin string) (*servicemodel.Organization, error) {
	responseOrganization, err := uc.schemeRepository.DeleteOrganization(oin)

	return responseOrganization, err
}

func (uc *SchemeUseCase) GetHealthChecks() []healthcheck.Checker {
	return []healthcheck.Checker{
		uc.schemeRepository,
	}
}
