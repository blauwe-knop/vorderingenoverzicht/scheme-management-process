// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"

	"go.uber.org/zap"

	serviceRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/repositories"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-management-process/internal/http_infra"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-management-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-management-process/pkg/events"
)

type options struct {
	ListenAddress        string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:80" description:"Address for the scheme-management-process api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	SchemeServiceAddress string `long:"scheme-service-address" env:"SCHEME_SERVICE_ADDRESS" default:"http://localhost:80" description:"Scheme service address."`
	SchemeServiceAPIKey  string `long:"scheme-service-api-key" env:"SCHEME_SERVICE_API_KEY" default:"" description:"API key to use when calling the scheme service."`
	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	schemeRepository := serviceRepositories.NewSchemeClient(cliOptions.SchemeServiceAddress, cliOptions.SchemeServiceAPIKey)

	schemeUseCase := usecases.NewSchemeUseCase(logger, schemeRepository)

	router := http_infra.NewRouter(schemeUseCase, logger)

	event := events.SMP_1
	logger.Log(event.GetLogLevel(), event.Message, zap.String("listenAddress", cliOptions.ListenAddress), zap.Reflect("event", event))

	logger.Info(fmt.Sprintf("start listening on %s", cliOptions.ListenAddress))
	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		event = events.SMP_2
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		if !errors.Is(err, http.ErrServerClosed) {
			panic(err)
		}
	}
}

func newLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
